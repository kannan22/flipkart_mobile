package flipkart_homepage;



import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.rules.ExpectedException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


public class Flipkart_main {
@Test
	public void flip() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		WebDriverWait wait = new WebDriverWait(driver, 20);
		
		driver.get("https://www.flipkart.com/");
		driver.findElementByXPath("//button[@class='_2AkmmA _29YdH8']").click();
		
	Actions builder = new Actions(driver);
	builder.moveToElement(driver.findElementByXPath("//span[text()='Electronics']")).perform();
	
	wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Mi"))).click();
	
	Thread.sleep(5000);
	String title = driver.getTitle();
	if (title.contains("Mi Mobile Phones"))
	System.out.println("Title is verified");
	
	driver.findElementByXPath("//div[text()='Newest First']").click();

	Thread.sleep(5000);
 List<WebElement> productname = driver.findElementsByXPath("//div[@class='_3wU53n']");
 List<WebElement> price = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']");
 
 for (int i= 0 ; i < productname.size();i++)
 {
	 
	 System.out.println(productname.get(i).getText()+" : "+price.get(i).getText());
	 
	 

 }
 
 Thread.sleep(2000);
 driver.findElementByXPath("//div[@class='_3wU53n']").click();

 Thread.sleep(3000);
Set<String> set = driver.getWindowHandles();
List<String> lst = new ArrayList<>();
lst.addAll(set);
driver.switchTo().window(lst.get(1));



 String titlenew = driver.getTitle();
	if (titlenew.contains("Redmi 6 Pro")) 
	System.out.println("Title is verified");
	else
	{
		System.out.println("Title is not verified");
	}
	
	String rating = driver.findElementByXPath("//span[@class='_38sUEc']//span//span[text()[contains(.,'Ratings')]]").getText();
	System.out.println("rating : "+ rating );
	String review = driver.findElementByXPath("//span[@class='_38sUEc']//span//span[text()[contains(.,'Reviews')]]").getText();
	System.out.println("review : "+ review );
	
		driver.quit();
	}

}
